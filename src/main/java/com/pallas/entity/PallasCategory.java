/**
 * 
 */
package com.pallas.entity;

import javax.persistence.Entity;

import com.pallas.setup.Auditable;

/**
 * @author Goku
 *
 */
@Entity
public class PallasCategory  extends Auditable<String> {
	public String category;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
