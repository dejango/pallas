/**
 * 
 */
package com.pallas.util;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.pallas.setup.AppUser;

/**
 * @author Goku
 *
 */
public class SessionUtil {
	public static Integer getClintId() {
		if(null!=SecurityContextHolder.getContext().getAuthentication() 
				&& SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof AppUser) {
			AppUser user =  (AppUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if(null!=user) {
				System.out.println("SessionUtil.getClintId() "+user.getClientId());
				return user.getClientId();
			}
		}
		return null;
	}
	public static AppUser getUser() {
		Object obj =SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(obj instanceof AppUser) {
			return (AppUser)obj;
		}
		return null;
	}
	public static HttpSession getSession() {
	    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    System.out.println(attr.getSessionId());
	    return attr.getRequest().getSession(false); 
	}
}
