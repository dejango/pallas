/**
 * 
 */
package com.pallas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pallas.entity.PallasUser;
import com.pallas.service.PallasUserService;
/**
 * @author Goku
 *
 */
@RestController
public class PallasUserController {
	
	@Autowired PallasUserService service;
	
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/saveAllPallasUser", method = RequestMethod.POST)
	public Long saveAllPallasUser(@RequestBody List<PallasUser> obj) {
		Iterable<PallasUser> r= service.saveAllPallasUser(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/savePallasUser", method = RequestMethod.POST)
	public Long savePallasUser(@RequestBody PallasUser obj) {
		PallasUser  r = service.savePallasUser(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updatePallasUser", method = RequestMethod.POST)
	public Long updatePallasUser(@RequestBody PallasUser obj) {
		PallasUser  r = service.savePallasUser(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updateAllPallasUser", method = RequestMethod.POST)
	public Long updateAllPallasUser(@RequestBody List<PallasUser> obj) {
		Iterable<PallasUser> r= service.saveAllPallasUser(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getPallasUser/{id}", method = RequestMethod.GET)
	public PallasUser getPallasUser(@PathVariable Long id) {
		return service.findByIdAndClientId(id);
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getAllPallasUser", method = RequestMethod.GET)
	public Iterable<PallasUser> getAllPallasUser() {
		return service.findByClientId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'DELETE')")
	@RequestMapping(value = "/deletePallasUser/{id}", method = RequestMethod.GET)
	public Integer deletePallasUser(@PathVariable Long id) {
		return service.deleteByIdAndClientId(id);
	}
}
