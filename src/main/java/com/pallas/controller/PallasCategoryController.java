/**
 * 
 */
package com.pallas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pallas.entity.PallasCategory;
import com.pallas.service.PallasCategoryService;
/**
 * @author Goku
 *
 */
@RestController
public class PallasCategoryController {
	
	@Autowired PallasCategoryService service;
	
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/saveAllPallasCategory", method = RequestMethod.POST)
	public Long saveAllPallasCategory(@RequestBody List<PallasCategory> obj) {
		Iterable<PallasCategory> r= service.saveAllPallasCategory(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/savePallasCategory", method = RequestMethod.POST)
	public Long savePallasCategory(@RequestBody PallasCategory obj) {
		PallasCategory  r = service.savePallasCategory(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updatePallasCategory", method = RequestMethod.POST)
	public Long updatePallasCategory(@RequestBody PallasCategory obj) {
		PallasCategory  r = service.savePallasCategory(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updateAllPallasCategory", method = RequestMethod.POST)
	public Long updateAllPallasCategory(@RequestBody List<PallasCategory> obj) {
		Iterable<PallasCategory> r= service.saveAllPallasCategory(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getPallasCategory/{id}", method = RequestMethod.GET)
	public PallasCategory getPallasCategory(@PathVariable Long id) {
		return service.findByIdAndClientId(id);
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getAllPallasCategory", method = RequestMethod.GET)
	public Iterable<PallasCategory> getAllPallasCategory() {
		return service.findByClientId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'DELETE')")
	@RequestMapping(value = "/deletePallasCategory/{id}", method = RequestMethod.GET)
	public Integer deletePallasCategory(@PathVariable Long id) {
		return service.deleteByIdAndClientId(id);
	}
}
