/**
 * 
 */
package com.pallas.setup;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Goku
 *
 */
public class GenericUtils {
	
	public static Map<String,String> response(int status, String eCode) {
		Map<String,String> resp = new HashMap<String,String>();
		if(status==0) {
			resp.put("Status", "FAILED");
			resp.put("Msg", eCode);
		}
		if(status==1) {
			resp.put("Status", "SUCCESS");
			resp.put("Msg", eCode);
		}
		return resp;
	}
	public static Map<String, Object> response(Object data) {
		Map<String,Object> resp = new HashMap<String,Object>();
		resp.put("data", data);
		return resp;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
