package com.pallas.service;

import java.util.List;

import com.pallas.entity.Client;

public interface ClientService {
	Client saveClient(Client obj);
	Iterable<Client> saveAllClient(List<Client> obj);
	void deleteById(Long id);
	Client findById(Long id);
}