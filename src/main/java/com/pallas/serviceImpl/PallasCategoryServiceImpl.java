/**
 * 
 */
package com.pallas.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pallas.entity.PallasCategory;
import com.pallas.repo.PallasCategoryRepo;
import com.pallas.service.PallasCategoryService;
import com.pallas.util.SessionUtil;
/**
 * @author Goku
 *
 */
@Service
public class PallasCategoryServiceImpl implements PallasCategoryService{

	@Autowired PallasCategoryRepo repo;
	
	@Override
	public Iterable<PallasCategory> saveAllPallasCategory(List<PallasCategory> obj) {
		return repo.saveAll(obj);
	}
	@Override
	public PallasCategory savePallasCategory(PallasCategory obj) {
		return repo.save(obj);
	}
	
	@Override
	public PallasCategory findByIdAndClientId(Long id) {
		return repo.findByIdAndClientId(id, SessionUtil.getClintId()).orElse(new PallasCategory());
	}
	
	@Override
	public Iterable<PallasCategory> findByClientId() {
		return repo.findByClientId(SessionUtil.getClintId());
	}
	@Override
	@Transactional
	public Integer deleteByIdAndClientId(Long id) {
		return repo.deleteByIdAndClientId(id, SessionUtil.getClintId());
	}
}
