/**
 * 
 */
package com.pallas.setup;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Goku
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
//@IdClass(PK.class)
public class Auditable<U> {
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY)
	@JsonProperty("id")
	public Long id;
    @JsonProperty
    @Column(updatable=false, nullable=false, name="client_id")
 //   @Id
    public Integer clientId;
    @CreatedBy
    @JsonProperty("createdBy")
    @Column(updatable=false, nullable=false)
    protected U createdBy;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("creationDate")
    @Column(updatable=false, nullable=false)
    protected Date creationDate;
    @LastModifiedBy
    @JsonProperty("lastModifiedBy")
    protected U lastModifiedBy;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("lastModifiedDate")
    protected Date lastModifiedDate;

	public U getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(U createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public U getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(U lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	@PrePersist
	@PreUpdate
    public void prePersist() {
		if(null!=SecurityContextHolder.getContext() && null!=SecurityContextHolder.getContext().getAuthentication()) {
			AppUser user =  (AppUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			this.setClientId(user.getClientId());
		}
    }
}
