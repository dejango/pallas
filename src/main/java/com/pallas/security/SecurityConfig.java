/**
 * 
 */
package com.pallas.security;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @author Goku
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	 @Bean
	    public CorsConfigurationSource corsConfigurationSource() {
	        CorsConfiguration configuration = new CorsConfiguration();
	        configuration.setAllowedOrigins(Arrays.asList("*","https://localhost:8081","https://127.0.0.1:8081"));
	        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
	        configuration.setAllowedHeaders(Arrays.asList("Authorization","Content-Type","authorization", "content-type", "x-auth-token","Set-Cookie"));
	        configuration.setExposedHeaders(Arrays.asList("x-auth-token","Set-Cookie"));
	        configuration.setAllowCredentials(true);
	        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        source.registerCorsConfiguration("/**", configuration);
	        return source;
	    }

/*	 @Override
	 public void configure(WebSecurity web) throws Exception {
		 web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	 }
*/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	 http.cors()
    	 .and().sessionManagement().sessionFixation().none()
    	 .and().csrf().disable().authorizeRequests()
    	 .antMatchers("/login**","/registerClient/**","/sms/**","/payment/**","/getPlans/**","/registerNewMember/**","/anonymous**","/anonymous/**").permitAll()
    	 .and().authorizeRequests().anyRequest().authenticated()
    	 .and().httpBasic()
    	// .and().formLogin().loginPage("/login").permitAll().usernameParameter("username").passwordParameter("password")
    	 .and().logout().logoutUrl("/logout").clearAuthentication(true)//.logoutSuccessUrl(null)
         .deleteCookies("JSESSIONID")
         .invalidateHttpSession(true);
    	 /*http.csrf().disable().authorizeRequests()
        .antMatchers("/").permitAll()
        .antMatchers(HttpMethod.POST, "/login").permitAll()
        .anyRequest().authenticated();*/
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
       // web.ignoring().antMatchers("/registerClient/**","/sms/**","/payment/**");
    }
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}