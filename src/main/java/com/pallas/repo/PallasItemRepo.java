/**
 * 
 */
package com.pallas.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pallas.entity.PallasItem;


/**
 * @author Goku
 *
 */
public interface PallasItemRepo extends CrudRepository<PallasItem, Long>{
	Optional<PallasItem> findByIdAndClientId(Long id, Integer clientId);
	Iterable<PallasItem> findByClientId(Integer clientId);
	Integer deleteByIdAndClientId(Long id, Integer clientId);
}
