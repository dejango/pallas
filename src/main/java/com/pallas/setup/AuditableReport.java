/**
 * 
 */
package com.pallas.setup;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Goku
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditableReport<U> {
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY)
	@JsonProperty("id")
	public Long id;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("creationDate")
    @Column(updatable=false, nullable=false)
    protected Date creationDate;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("lastModifiedDate")
    protected Date lastModifiedDate;

	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
