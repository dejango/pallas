/**
 * 
 */
package com.pallas.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pallas.entity.PallasItem;
import com.pallas.repo.PallasItemRepo;
import com.pallas.service.PallasItemService;
import com.pallas.util.SessionUtil;
/**
 * @author Goku
 *
 */
@Service
public class PallasItemServiceImpl implements PallasItemService{

	@Autowired PallasItemRepo repo;
	
	@Override
	public Iterable<PallasItem> saveAllPallasItem(List<PallasItem> obj) {
		return repo.saveAll(obj);
	}
	@Override
	public PallasItem savePallasItem(PallasItem obj) {
		return repo.save(obj);
	}
	
	@Override
	public PallasItem findByIdAndClientId(Long id) {
		return repo.findByIdAndClientId(id, SessionUtil.getClintId()).orElse(new PallasItem());
	}
	
	@Override
	public Iterable<PallasItem> findByClientId() {
		return repo.findByClientId(SessionUtil.getClintId());
	}
	@Override
	@Transactional
	public Integer deleteByIdAndClientId(Long id) {
		return repo.deleteByIdAndClientId(id, SessionUtil.getClintId());
	}
}
