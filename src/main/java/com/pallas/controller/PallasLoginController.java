/**
 * 
 */
package com.pallas.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pallas.entity.PallasUser;
import com.pallas.service.PallasUserService;
import com.pallas.setup.AppUser;
import com.pallas.util.SessionUtil;

/**
 * @author Goku
 *
 */
@RestController
public class PallasLoginController {
	
	@Autowired PallasUserService users;
	Logger logger = LogManager.getLogger(PallasLoginController.class);
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public Map<String,String> login() {
		logger.info("==========================================");
		Map<String,String> resp = new HashMap<String,String>();
		AppUser user = SessionUtil.getUser();
		if(null!=user.getRole() && user.getRole().equalsIgnoreCase("MEMBER")) {
			PallasUser u = users.findByIdAndClientId(user.getId());
			logger.info("Member Login........"+u);
		}
		resp.put("ROLE", user.getRole());
		return resp;
	}

}
