/**
 * 
 */
package com.pallas.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pallas.setup.GenericUtils;

/**
 * @author Goku
 *
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {
	@ExceptionHandler(value = {SQLIntegrityConstraintViolationException.class,
			DataIntegrityViolationException.class, ConstraintViolationException.class,
			TransactionSystemException.class,PersistenceException.class,UnexpectedRollbackException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody Map<String,String> deniedPermissionException(Exception ex) {
		System.out.println(ExceptionUtils.getRootCauseMessage(ex));
		Map<String,String> m=new HashMap<>(); 
		if(StringUtils.containsIgnoreCase(ExceptionUtils.getRootCauseMessage(ex), "Duplicate")) {
			String temp = ExceptionUtils.getRootCauseMessage(ex);
			temp = StringUtils.substringBetween(temp, "'");
			m= GenericUtils.response(0,"Duplicate");
			m.put("key", temp);
		}else {
			m= GenericUtils.response(0,ExceptionUtils.getRootCauseMessage(ex));
		}
        return m;
    }
}
