/**
 * 
 */
package com.pallas.service;

import com.pallas.setup.AppUser;

/**
 * @author Goku
 *
 */
public interface CommonService {

	AppUser authenticate(String uname, String pwd);
}
