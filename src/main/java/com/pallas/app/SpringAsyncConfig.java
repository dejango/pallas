/**
 * 
 */
package com.pallas.app;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author Goku
 *
 */
@Configuration
@EnableAsync
public class SpringAsyncConfig {
    
    @Bean(name = "pallasAsync")
    public Executor threadPoolTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }
}