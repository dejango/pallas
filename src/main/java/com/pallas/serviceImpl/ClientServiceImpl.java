/**
 * 
 */
package com.pallas.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pallas.entity.Client;
import com.pallas.repo.ClientRepo;
import com.pallas.service.ClientService;
/**
 * @author Goku
 *
 */
@Service
public class ClientServiceImpl implements ClientService{

	@Autowired ClientRepo repo;
	
	@Override
	public Iterable<Client> saveAllClient(List<Client> obj) {
		return repo.saveAll(obj);
	}
	@Override
	public Client saveClient(Client obj) {
		return repo.save(obj);
	}
	
	@Override
	public Client findById(Long id) {
		return repo.findById(id).orElse(new Client());
	}
	
	@Override
	@Transactional
	public void deleteById(Long id) {
		repo.deleteById(id);
	}
}
