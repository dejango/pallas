/**
 * 
 */
package com.pallas.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Goku
 *
 */
@Entity
public class Client {
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY)
	@JsonProperty("id")
	private Long id;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("creationDate")
    @Column(updatable=false, nullable=false)
    private Date creationDate;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("lastModifiedDate")
    private Date lastModifiedDate;
    private String entity;
    private String status;
    private Date Expiry;
    private String authToken;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getExpiry() {
		return Expiry;
	}
	public void setExpiry(Date expiry) {
		Expiry = expiry;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
}
