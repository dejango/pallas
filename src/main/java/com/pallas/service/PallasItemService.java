package com.pallas.service;

import java.util.List;

import com.pallas.entity.PallasItem;

public interface PallasItemService {
	PallasItem savePallasItem(PallasItem obj);
	Iterable<PallasItem> saveAllPallasItem(List<PallasItem> obj);
	PallasItem findByIdAndClientId(Long id);
	Iterable<PallasItem> findByClientId();
	Integer deleteByIdAndClientId(Long id);
}