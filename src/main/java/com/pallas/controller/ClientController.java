/**
 * 
 */
package com.pallas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pallas.entity.Client;
import com.pallas.service.ClientService;
/**
 * @author Goku
 *
 */
@RestController
public class ClientController {
	
	@Autowired ClientService service;
	
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/saveAllClient", method = RequestMethod.POST)
	public Long saveAllClient(@RequestBody List<Client> obj) {
		Iterable<Client> r= service.saveAllClient(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/saveClient", method = RequestMethod.POST)
	public Long saveClient(@RequestBody Client obj) {
		Client  r = service.saveClient(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updateClient", method = RequestMethod.POST)
	public Long updateClient(@RequestBody Client obj) {
		Client  r = service.saveClient(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updateAllClient", method = RequestMethod.POST)
	public Long updateAllClient(@RequestBody List<Client> obj) {
		Iterable<Client> r= service.saveAllClient(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getClient/{id}", method = RequestMethod.GET)
	public Client getClient(@PathVariable Long id) {
		return service.findById(id);
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'DELETE')")
	@RequestMapping(value = "/deleteClient/{id}", method = RequestMethod.GET)
	public void deleteClient(@PathVariable Long id) {
		service.deleteById(id);
	}
}
