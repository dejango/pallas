/**
 * 
 */
package com.pallas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.pallas.setup.Auditable;


/**
 * @author Goku
 *
 */
@Entity
public class PallasUser extends Auditable<String>{
	private String status;
	private String name;
	@Column(unique=true)
	private String login;
	private String password;
	private String role;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}
