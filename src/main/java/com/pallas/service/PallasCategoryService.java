package com.pallas.service;

import java.util.List;

import com.pallas.entity.PallasCategory;

public interface PallasCategoryService {
	PallasCategory savePallasCategory(PallasCategory obj);
	Iterable<PallasCategory> saveAllPallasCategory(List<PallasCategory> obj);
	PallasCategory findByIdAndClientId(Long id);
	Iterable<PallasCategory> findByClientId();
	Integer deleteByIdAndClientId(Long id);
}