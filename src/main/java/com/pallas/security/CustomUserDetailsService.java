/**
 * 
 */
package com.pallas.security;

import java.util.Collection;

import javax.management.RuntimeErrorException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pallas.entity.Client;
import com.pallas.entity.PallasUser;
import com.pallas.repo.PallasUserRepo;
import com.pallas.service.ClientService;
import com.pallas.setup.AppUser;

/**
 * @author Goku
 *
 */
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private PallasUserRepo userRepository;
    @Autowired ClientService client;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    	System.out.println("CustomUserDetailsService.loadUserByUsername() ------");
        PallasUser user = userRepository.findByLogin(userName)
       .orElseThrow(() -> new UsernameNotFoundException("Name " + userName + " not found"));
        System.out.println(user);
        Client c = new Client();
        try {
        	c = client.findById(new Long(user.getClientId()));
        	return new AppUser(user.getName(), "{noop}"+user.getPassword(),
        	         getAuthorities(user), user.getClientId(), user.getRole(),user.getId(),c.getStatus(),user.getStatus());
        }catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
         return null;
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(PallasUser user) {
    	System.out.println("CustomUserDetailsService.getAuthorities() --- "+user);
    	if(user.getRole()==null) {
    		throw new RuntimeErrorException(null, "No roles defined for this user");
    	}
        String[] userRoles = user.getRole().split("\\s*,\\s*");
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }
}