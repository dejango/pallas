/**
 * 
 */
package com.pallas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pallas.entity.PallasItem;
import com.pallas.service.PallasItemService;
/**
 * @author Goku
 *
 */
@RestController
public class PallasItemController {
	
	@Autowired PallasItemService service;
	
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/saveAllPallasItem", method = RequestMethod.POST)
	public Long saveAllPallasItem(@RequestBody List<PallasItem> obj) {
		Iterable<PallasItem> r= service.saveAllPallasItem(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'CREATE')")
	@RequestMapping(value = "/savePallasItem", method = RequestMethod.POST)
	public Long savePallasItem(@RequestBody PallasItem obj) {
		PallasItem  r = service.savePallasItem(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updatePallasItem", method = RequestMethod.POST)
	public Long updatePallasItem(@RequestBody PallasItem obj) {
		PallasItem  r = service.savePallasItem(obj);
		return null==r?0:r.getId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'UPDATE')")
	@RequestMapping(value = "/updateAllPallasItem", method = RequestMethod.POST)
	public Long updateAllPallasItem(@RequestBody List<PallasItem> obj) {
		Iterable<PallasItem> r= service.saveAllPallasItem(obj);
		return null==r?0:r.spliterator().getExactSizeIfKnown();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getPallasItem/{id}", method = RequestMethod.GET)
	public PallasItem getPallasItem(@PathVariable Long id) {
		return service.findByIdAndClientId(id);
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'VIEW')")
	@RequestMapping(value = "/getAllPallasItem", method = RequestMethod.GET)
	public Iterable<PallasItem> getAllPallasItem() {
		return service.findByClientId();
	}
	@PreAuthorize("hasPermission('SETTINGS', 'ADMIN', 'DELETE')")
	@RequestMapping(value = "/deletePallasItem/{id}", method = RequestMethod.GET)
	public Integer deletePallasItem(@PathVariable Long id) {
		return service.deleteByIdAndClientId(id);
	}
}
