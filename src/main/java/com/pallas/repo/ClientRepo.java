/**
 * 
 */
package com.pallas.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pallas.entity.Client;


/**
 * @author Goku
 *
 */
public interface ClientRepo extends CrudRepository<Client, Long>{
	Optional<Client> findById(Long id); 
	void deleteById(Long id);
}
