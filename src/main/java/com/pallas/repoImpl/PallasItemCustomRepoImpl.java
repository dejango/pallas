/**
 * 
 */
package com.pallas.repoImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.pallas.repo.PallasItemCustomRepo;

/**
 * @author Goku
 *
 */
@Repository
public class PallasItemCustomRepoImpl implements PallasItemCustomRepo{
	@Autowired
	NamedParameterJdbcTemplate namedParamJdbcTemplate;
	@Autowired
    JdbcTemplate jdbcTemplate;
}
