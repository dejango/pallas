/**
 * 
 */
package com.pallas.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.pallas.repo.CommonRepo;
import com.pallas.service.CommonService;
import com.pallas.setup.AppUser;
import com.pallas.util.CacheHub;

/**
 * @author Goku
 *
 */
@Service
public class CommonServiceImpl implements CommonService{
	@Autowired CommonRepo repo;
	@Autowired AuthenticationManager authManager;
	@Autowired CacheHub cHub;

	
	@Override
	public AppUser authenticate(String uname, String pwd) {
		UsernamePasswordAuthenticationToken authReq
		 = new UsernamePasswordAuthenticationToken(uname, pwd);
		Authentication auth = authManager.authenticate(authReq);
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(auth);
		
		if(null!=auth) {
			AppUser user =  (AppUser)sc.getAuthentication().getPrincipal();
			System.out.println("CommonServiceImpl.authenticate()--- "+user);
			return user;
		}
		return null;
	}
}
