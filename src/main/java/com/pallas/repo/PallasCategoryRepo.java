/**
 * 
 */
package com.pallas.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pallas.entity.PallasCategory;


/**
 * @author Goku
 *
 */
public interface PallasCategoryRepo extends CrudRepository<PallasCategory, Long>{
	Optional<PallasCategory> findByIdAndClientId(Long id, Integer clientId);
	Iterable<PallasCategory> findByClientId(Integer clientId);
	Integer deleteByIdAndClientId(Long id, Integer clientId);
}
