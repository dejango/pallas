/**
 * 
 */
package com.pallas.setup;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import com.pallas.security.IAuthenticationFacade;

/**
 * @author Goku
 *
 */
@Service
public class AuditorAwareImpl implements AuditorAware<String> {

	@Autowired
	IAuthenticationFacade auth;
	
	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.ofNullable(null==auth.getAuthentication()?"UNKNOWN":auth.getAuthentication().getName());
	}
   
}