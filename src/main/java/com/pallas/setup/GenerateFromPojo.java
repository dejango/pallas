/**
 * 
 */
package com.pallas.setup;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

/**
 * @author Goku
 *
 */
public class GenerateFromPojo {
	
	public static void main(String[] args) {
		String name = "PallasCategory";
			try {
				createRepo(name);
				createService(name);
				createController(name);
				createCustRepo(name);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public static void createRepo(String fName) throws IOException {
		File f = new File("src/main/java/com/pallas/repo/",fName+"Repo.java");
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
		}else {
			return;
		}
		File repo = new File("src/main/resources/templates/repo.txt");
		String str = FileUtils.readFileToString(repo, StandardCharsets.UTF_8);
		
		str = str.replaceAll(":class",fName);
		FileUtils.write(f, str, StandardCharsets.UTF_8);
	}
	public static void createController(String fName) throws IOException {
		File f = new File("src/main/java/com/pallas/controller/",fName+"Controller.java");
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
		}else {
			return;
		}
		File f1 = new File("src/main/resources/templates/controller.txt");
		String str = FileUtils.readFileToString(f1, StandardCharsets.UTF_8);
		
		str = str.replaceAll(":class",fName);
		FileUtils.write(f, str, StandardCharsets.UTF_8);
	}
	public static void createService(String fName) throws IOException {
		File f = new File("src/main/java/com/pallas/service/",fName+"Service.java");
		File f1 = new File("src/main/java/com/pallas/serviceImpl/",fName+"ServiceImpl.java");
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
			File service = new File("src/main/resources/templates/service.txt");
			String str = FileUtils.readFileToString(service, StandardCharsets.UTF_8);
			str = str.replaceAll(":class",fName);
			FileUtils.write(f, str, StandardCharsets.UTF_8);
		}else {
			return;
		}
		if(!f1.exists()) {
			f1.getParentFile().mkdirs();
			f1.createNewFile();
			File serviceImpl = new File("src/main/resources/templates/serviceImpl.txt");
			String str1 = FileUtils.readFileToString(serviceImpl, StandardCharsets.UTF_8);
			str1 = str1.replaceAll(":class",fName);
			FileUtils.write(f1, str1, StandardCharsets.UTF_8);
		}else {
			return;
		}
		
	}
	public static void createCustRepo(String fName) throws IOException {
		File f = new File("src/main/java/com/pallas/repo/",fName+"CustomRepo.java");
		File f1 = new File("src/main/java/com/pallas/repoImpl/",fName+"CustomRepoImpl.java");
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
			File service = new File("src/main/resources/templates/custRepo.txt");
			String str = FileUtils.readFileToString(service, StandardCharsets.UTF_8);
			str = str.replaceAll(":class",fName);
			FileUtils.write(f, str, StandardCharsets.UTF_8);
		}else {
			return;
		}
		if(!f1.exists()) {
			f1.getParentFile().mkdirs();
			f1.createNewFile();
			File serviceImpl = new File("src/main/resources/templates/custRepoImpl.txt");
			String str1 = FileUtils.readFileToString(serviceImpl, StandardCharsets.UTF_8);
			str1 = str1.replaceAll(":class",fName);
			FileUtils.write(f1, str1, StandardCharsets.UTF_8);
		}else {
			return;
		}
		
	}
}
