/**
 * 
 */
package com.pallas.repoImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.pallas.repo.CommonRepo;

/**
 * @author Goku
 *
 */
@Repository
public class CommonRepoImpl implements CommonRepo{
	@Autowired
	NamedParameterJdbcTemplate namedParamJdbcTemplate;
	@Autowired
    JdbcTemplate jdbcTemplate;
	
	
}
