/**
 * 
 */
package com.pallas.setup;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Goku
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class OnlyAuditable<U> {
    @CreatedBy
    @JsonProperty("createdBy")
    @Column(updatable=false)
    protected U createdBy;
    @CreatedDate
    @Temporal(TemporalType.DATE)
    @Column(updatable=false)
    @JsonProperty("creationDate")
    protected Date creationDate;
    @LastModifiedBy
    @JsonProperty("lastModifiedBy")
    protected U lastModifiedBy;
    @LastModifiedDate
    @Temporal(TemporalType.DATE)
    @JsonProperty("lastModifiedDate")
    protected Date lastModifiedDate;

	public U getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(U createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public U getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(U lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
