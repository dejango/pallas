/**
 * 
 */
package com.pallas.security;

import org.springframework.security.core.Authentication;

/**
 * @author Goku
 *
 */
public interface IAuthenticationFacade {
    Authentication getAuthentication();
}