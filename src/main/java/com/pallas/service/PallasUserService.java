package com.pallas.service;

import java.util.List;

import com.pallas.entity.PallasUser;

public interface PallasUserService {
	PallasUser savePallasUser(PallasUser obj);
	Iterable<PallasUser> saveAllPallasUser(List<PallasUser> obj);
	PallasUser findByIdAndClientId(Long id);
	Iterable<PallasUser> findByClientId();
	Integer deleteByIdAndClientId(Long id);
	PallasUser findByTag(Long tagId);
}