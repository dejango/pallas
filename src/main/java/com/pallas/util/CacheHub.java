/**
 * 
 */
package com.pallas.util;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;

/**
 * @author Goku
 *
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
public class CacheHub {
	ExpiringMap<String, Object> tagCache = ExpiringMap.builder()
			  .expirationPolicy(ExpirationPolicy.CREATED)
			  .expiration(15, TimeUnit.MINUTES)
			  .build(); 
	public ExpiringMap<String, Object> getCache(String cache){
		if(StringUtils.equalsIgnoreCase(cache, "tagCache")) {
			return tagCache;
		}
		return null;
	}
}
