/**
 * 
 */
package com.pallas.entity;

import javax.persistence.Entity;

import com.pallas.setup.Auditable;

/**
 * @author Goku
 *
 */
@Entity
public class PallasItem  extends Auditable<String> {
	private String title;
	private Double price;
	private Long categoryId;
	private String cid;
	private String qty;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
}
