/**
 * 
 */
package com.pallas.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pallas.entity.PallasUser;
import com.pallas.repo.PallasUserCustomRepo;
import com.pallas.repo.PallasUserRepo;
import com.pallas.service.PallasUserService;
import com.pallas.util.SessionUtil;
/**
 * @author Goku
 *
 */
@Service
public class PallasUserServiceImpl implements PallasUserService{

	@Autowired PallasUserRepo repo;
	@Autowired PallasUserCustomRepo custRepo;
	
	@Override
	public Iterable<PallasUser> saveAllPallasUser(List<PallasUser> obj) {
		return repo.saveAll(obj);
	}
	@Override
	public PallasUser savePallasUser(PallasUser obj) {
		return repo.save(obj);
	}
	
	@Override
	public PallasUser findByIdAndClientId(Long id) {
		return repo.findByIdAndClientId(id, SessionUtil.getClintId()).orElse(new PallasUser());
	}
	
	@Override
	public Iterable<PallasUser> findByClientId() {
		return repo.findByClientId(SessionUtil.getClintId());
	}
	@Override
	@Transactional
	public Integer deleteByIdAndClientId(Long id) {
		return repo.deleteByIdAndClientId(id, SessionUtil.getClintId());
	}
	@Override
	public PallasUser findByTag(Long tagId) {
		// TODO Auto-generated method stub
		return null;
	}
}
