/**
 * 
 */
package com.pallas.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pallas.entity.PallasUser;


/**
 * @author Goku
 *
 */
public interface PallasUserRepo extends CrudRepository<PallasUser, Long>{
	Optional<PallasUser> findByIdAndClientId(Long id, Integer clientId);
	Iterable<PallasUser> findByClientId(Integer clientId);
	Integer deleteByIdAndClientId(Long id, Integer clientId);
	Optional<PallasUser> findByLogin(String login);
}
