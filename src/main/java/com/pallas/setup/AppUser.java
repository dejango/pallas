/**
 * 
 */
package com.pallas.setup;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Goku
 *
 */
public class AppUser extends org.springframework.security.core.userdetails.User{
	private Long id;
	private Integer clientId;
	private String role;
	private String status;
	private String userStatus;
	public AppUser(String username, String password, Collection<? extends GrantedAuthority> authorities, 
			Integer clientId, String role, Long id,String status,String userStatus) {
		super(username, password, authorities);
		this.clientId=clientId;
		this.role=role;
		this.id=id;
		this.status=status;
		this.userStatus=userStatus;
	}
	private static final long serialVersionUID = -1368168912715717732L;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	@Override
	public String toString() {
		return "AppUser [id=" + id + ", clientId=" + clientId + ", role=" + role + ", status=" + status
				+ ", userStatus=" + userStatus + "]";
	}
}
