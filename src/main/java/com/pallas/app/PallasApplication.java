package com.pallas.app;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan(basePackages={"com"})
@EntityScan(basePackages={"com"}) 
@EnableJpaRepositories(basePackages={"com"}) 
@EnableMBeanExport(registration=RegistrationPolicy.IGNORE_EXISTING)
@SpringBootApplication
@EnableScheduling
public class PallasApplication extends SpringBootServletInitializer{
	@Value("${server.tomcat.ajp.port}")
    int ajpPort;

    @Value("${server.tomcat.ajp.enabled}")
    boolean tomcatAjpEnabled;
	public static void main(String[] args) {
		SpringApplication.run(PallasApplication.class, args);
	}
	/*@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }
    private static Class<PallasApplication> applicationClass = PallasApplication.class;*/
	@Bean
	public TomcatServletWebServerFactory servletContainer() {

	    TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
	    if (tomcatAjpEnabled)
	    {
	        Connector ajpConnector = new Connector("AJP/1.3");
	        ajpConnector.setPort(ajpPort);
	        ajpConnector.setSecure(false);
	        ajpConnector.setAllowTrace(false);
	        ajpConnector.setScheme("https");
	        tomcat.addAdditionalTomcatConnectors(ajpConnector);
	    }
	    return tomcat;
	  }
}
